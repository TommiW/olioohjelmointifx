/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioohjelmointifx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author tommi
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button1;
    @FXML
    private TextField textField;
    
    @FXML
    private void Button1Action(ActionEvent event) {
        label.setText(label.getText() + "\n");
        textField.clear();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void updateFromTextField(KeyEvent event) {
//                System.out.println("textfield");
                label.setText(label.getText()+(event.getCharacter()));
    }

    @FXML
    private void textFieldAction(ActionEvent event) {
//        label.setText(label.getText()+textField.getText());
//        label.setText(label.getText() +textField.getText()+ "\n");
//        label.setText(label.getText());
        textField.clear();
    }

    @FXML
    private void backSpaceAction(KeyEvent event) {
        if (event.getCode().equals(event.getCode().BACK_SPACE))
        {
            System.out.println("backspace");
            if (label.getText().length() > 0) {
                label.setText(label.getText().substring(0,label.getText().length()-1));
            }
        }
    }
    
}
